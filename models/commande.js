module.exports = (sequelize, Sequelize) => {
    const Commande = sequelize.define("commandes", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nom_produits: {
            type: Sequelize.STRING,
        },
        prix: {
            type: Sequelize.FLOAT
        },
        status: {
            type: Sequelize.STRING
        },
        type: {
            type: Sequelize.STRING
        }
    });

    return Commande;
};