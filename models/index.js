const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
    config.DB,
    config.USER,
    config.PASSWORD, {
        host: config.HOST,
        dialect: config.dialect,
        operatorsAliases: false,

        pool: {
            max: config.pool.max,
            min: config.pool.min,
            acquire: config.pool.acquire,
            idle: config.pool.idle
        }
    }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/user.model.js")(sequelize, Sequelize);
//db.role = require("../models/role.model.js")(sequelize, Sequelize);
db.produit = require("../models/produit.model")(sequelize, Sequelize);
db.commande = require("../models/commande.model")(sequelize, Sequelize);

db.produit.belongsToMany(db.commande, {
    through: "commande_produit",
    as: "produits",
    foreignKey: "produit_id"
})

db.commande.belongsToMany(db.produit, {
    through: "commande_produit",
    as: "commandes",
    foreignKey: "commande_id"
})

db.role.hasMany(db.user, { as: "users", foreignKey: "role"});
db.user.belongsTo(db.role, {
    foreignKey: "role",
    as: "roles",
    otherKey: "id"
})

module.exports = db;
