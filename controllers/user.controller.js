//dépendances
const db = require("../models");
const bcrypt = require("bcrypt");

//function for users
exports.getAll = async (req, res) => {
    let users = await db.user.findAll({attributes: ['id', 'nom', 'prenom'], include: ["roles"]});
    res.status(200).json(users)
}

exports.getById = async (req, res) => {
    let id = req.params.id;
    let user = await db.user.findOne({attributes: ['id', 'nom', 'prenom'], include: ["roles"] , where: {id: id}})
    res.status(200).json(user)
}

exports.createUser = async (req, res) => {
    const { prenom, nom, username, password, role} = req.body;
    let hashedPassword = bcrypt.hashSync(password, 10);
    await db.user.create({ prenom: "", nom: "", password: hashedPassword, username: username, role: 1})
        .then(user => res.send({message: 'User created succesfully', error: 2}));
}

exports.createUserWeb = async (req, res) => {
    const { prenom, nom, username, password, role} = req.body;
    let hashedPassword = bcrypt.hashSync(password, 10);
    console.log(hashedPassword)
    await db.user.create({ prenom: "", nom: "", password: hashedPassword, username: username, role: 1})
        .then(user => res.render("pages/login", {message: 'User created succesfully', error: 2}));
}

exports.update = async (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        })
    }

    await db.user.update(
        req.body
    , {
        where: {
            id: req.params.id
        }
    }).then(user => res.json({user, msg: 'User updated succesfully'}))
    .catch(res.status(500).send({message: "error with the json data"}))
}

exports.delete = async (req, res) => {
    await db.user.destroy({
        where: {
            id: req.params.id
        }
    })
}

//fonctions to test auth
exports.allAccess = (req, res) => {
    res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
    res.status(200).send("User Content.");
};

exports.adminBoard = (req, res) => {
    res.status(200).send("Admin Content.");
};