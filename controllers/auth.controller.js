//dépendences
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

//chargement des modèles
const db = require("../models");
//chargement des configs
const config = require("../config/auth.config");
const User = db.user;
const Role = db.role;
const Op = db.Sequelize.Op;

//module d'auth
exports.signin = (req, res) => {
  User.findOne({
    where: {
      username: req.body.username
    }
  })
    .then(user => {
      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }
      let passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );
      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!"
        });
      }

      let token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400 // 24 hours
      });

      let authorities = "";
      if (user.role === 1) {
          authorities = "user"
      } else {
          authorities = "asmin"
      }
        res.status(200).send({
          id: user.id,
          username: user.username,
          role: authorities,
          accessToken: token
        });
      })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};