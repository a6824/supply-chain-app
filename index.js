const express = require("express");
require("dotenv").config();
"use strict";
const nodemailer = require("nodemailer");
const http = require("http")
const cors = require('cors')

//variables nécessaire
const app = express()
const port = 3000

// chargement des models
const db = require("./models")

// synchronization de la base
db.sequelize.sync()
    .then(() => {
        console.log('successfully sync base')
    })

app.set("view engine", "ejs");
app.set("views", "./views");

app.use(express.static("views/images"))

// enabling cors request
app.use(cors())

// appel des routes api
require('./routes/auth.routes')(app)
require('./routes/user.routes')(app)
//require('./routes/image.routes')(app)
require('./routes/produit.routes')(app)
require('./routes/commande.routes')(app)

//appel des routes de vue
require('./routes/view.routes')(app)

//default routes de test
app.get("/api/readinnes", function (req, res) {
    res.json("hello world")
})

//lancement serveur
console.log(`listening on port ${port}`)
http.createServer(app).listen(port)
