//dépendances
const db = require("../models");
const User = db.user;

checkDuplicateUsername = (req, res, next) => {
    // Username
    User.findOne({
        where: {
            username: req.body.username
        }
    }).then(user => {
        if (user == null) {
            throw ''
        }
        res.status(400).send({
                message: "Failed! Username is already in use!"
            });
        })
        .catch(() => {
            next()
        });
}

checkDuplicateUsernameWeb = (req, res, next) => {
    // Username
    User.findOne({
        where: {
            username: req.body.username
        }
    }).then(user => {
        if (user == null) {
            throw ''
        }
        res.render("pages/register", {error:1, message:"username already in use"})
    })
        .catch(() => {
            next()
        });
};

const verifySignUp = {
    checkDuplicateUsername: checkDuplicateUsername,
    checkDuplicateUsernameWeb: checkDuplicateUsernameWeb
};

module.exports = verifySignUp;
