//dépendances
const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const User = db.user;

verifyToken = (req, res, next) => {
    let token = req.headers["x-access-token"];

    if (!token) {
        return res.status(403).send({
            message: "No token provided!"
        });
    }

    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            return res.status(401).send({
                message: "Unauthorized!"
            });
        }
        req.userId = decoded.id;
        next();
    });
};

verifyTokenWeb = (req, res, next) => {
    let token = req.cookies["accessToken"];

    if (!token) {
        return res.render('pages/login', {error: 1, message: "no token provided"})
    }

    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            return res.render('pages/login', {error:1, message: "token is invalid"});
        }
        req.userId = decoded.id;
        next();
    });
};

isAdmin = (req, res, next) => {
    User.findByPk(req.userId).then(user => {

        if (user.role === 1) {
            next();
        }

        res.status(403).send({
            message: "Require Admin Role!"
        });
    });
};

const authJwt = {
    verifyToken: verifyToken,
    verifyTokenWeb: verifyTokenWeb,
    isAdmin: isAdmin
};

module.exports = authJwt;
