const { authJwt, verifySignUp } = require("../middleware");
const produitController = require("../controllers/produit.controller");
const commandeController = require("../controllers/commande.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/", authJwt.verifyTokenWeb, function(req, res) {
        res.render('pages/index')
    })

    app.get("/login", function(req, res) {
        res.render('pages/login', {error: 3, message: ""})
    });

    app.get("/register", function(req, res) {
        res.render('pages/register')
    });

    app.get("/produits", authJwt.verifyTokenWeb, produitController.getAllWeb)

    app.get("/sendProducts/validate", authJwt.verifyTokenWeb, function(req, res) {
        res.render("pages/sendProducts/validate")
    })
    app.put("/sendProducts/validate/:id", [authJwt.verifyToken, authJwt.isAdmin], commandeController.validate);

    app.get("/followProducts/validateRecep", authJwt.verifyTokenWeb, function(req, res) {
        res.render("pages/followProducts/validateRecep")
    })
    app.put("/followProducts/validateRecep/:id", [authJwt.verifyToken, authJwt.isAdmin], commandeController.validateRecep);
};