const commandeController = require("../controllers/commande.controller");
const { authJwt, verifySignUp } = require("../middleware");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post("/api/commandes",  [authJwt.verifyToken, authJwt.isAdmin, verifySignUp.checkDuplicateUsername], commandeController.createCommande);

    app.get("/api/commandes", commandeController.getAll);

    app.get("/api/commandes/:id", commandeController.getById);

    app.get("/api/commandes/status", commandeController.getByStatus);

    app.put("/api/commandes/:id", [authJwt.verifyToken, authJwt.isAdmin], commandeController.updateCommande);

    app.delete("/api/commandes/:id", [authJwt.verifyToken, authJwt.isAdmin], commandeController.delete)

};