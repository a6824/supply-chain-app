const produitController = require("../controllers/produit.controller");
const { authJwt, verifySignUp } = require("../middleware");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post("/api/produits",  [authJwt.verifyToken, authJwt.isAdmin, verifySignUp.checkDuplicateUsername], produitController.createProduit);

    app.get("/api/produits", produitController.getAll);

    app.get("/api/produits/:id", produitController.getById);

    app.put("/api/produits/:id", [authJwt.verifyToken, authJwt.isAdmin], produitController.updateProduit);

    app.delete("/api/produits/:id", [authJwt.verifyToken, authJwt.isAdmin], produitController.delete)

};